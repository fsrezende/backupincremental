﻿namespace BackupIncremental.v._01
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btVarrer = new System.Windows.Forms.Button();
            this.tbDestino = new System.Windows.Forms.TextBox();
            this.tbOrigem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbInfo = new System.Windows.Forms.Label();
            this.btBackup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(8, 80);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(307, 23);
            this.progressBar1.TabIndex = 11;
            // 
            // btVarrer
            // 
            this.btVarrer.Location = new System.Drawing.Point(165, 4);
            this.btVarrer.Name = "btVarrer";
            this.btVarrer.Size = new System.Drawing.Size(71, 23);
            this.btVarrer.TabIndex = 10;
            this.btVarrer.Text = "Varrer";
            this.btVarrer.UseVisualStyleBackColor = true;
            this.btVarrer.Click += new System.EventHandler(this.btVarrer_Click);
            // 
            // tbDestino
            // 
            this.tbDestino.Location = new System.Drawing.Point(59, 33);
            this.tbDestino.Name = "tbDestino";
            this.tbDestino.Size = new System.Drawing.Size(100, 20);
            this.tbDestino.TabIndex = 9;
            this.tbDestino.Text = "f:\\";
            // 
            // tbOrigem
            // 
            this.tbOrigem.Location = new System.Drawing.Point(59, 6);
            this.tbOrigem.Name = "tbOrigem";
            this.tbOrigem.Size = new System.Drawing.Size(100, 20);
            this.tbOrigem.TabIndex = 8;
            this.tbOrigem.Text = "d:\\";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Destino:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Origem:";
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(12, 56);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(96, 13);
            this.lbInfo.TabIndex = 12;
            this.lbInfo.Text = "dados da análise...";
            // 
            // btBackup
            // 
            this.btBackup.Location = new System.Drawing.Point(242, 4);
            this.btBackup.Name = "btBackup";
            this.btBackup.Size = new System.Drawing.Size(73, 23);
            this.btBackup.TabIndex = 13;
            this.btBackup.Text = "Backup";
            this.btBackup.UseVisualStyleBackColor = true;
            this.btBackup.Click += new System.EventHandler(this.btBackup_Click);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 114);
            this.Controls.Add(this.btBackup);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btVarrer);
            this.Controls.Add(this.tbDestino);
            this.Controls.Add(this.tbOrigem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPrincipal";
            this.Text = "Backup Incremental v0.01";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btVarrer;
        public System.Windows.Forms.TextBox tbDestino;
        public System.Windows.Forms.TextBox tbOrigem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Button btBackup;


    }
}

