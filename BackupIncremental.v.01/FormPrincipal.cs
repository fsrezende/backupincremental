﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Permissions;
using System.Runtime.CompilerServices;

namespace BackupIncremental.v._01
{
    public partial class FormPrincipal : Form
    {
        Thread t;

        public delegate void AddProgressDelegate(int valor);
        public AddProgressDelegate addProgressDelegate;

        public delegate void SetInfoDelegate(string info);
        public SetInfoDelegate setInfoDelegate;

        ProcessarOrigemEDestinoThree pod;

        public FormPrincipal()
        {
            InitializeComponent();
            addProgressDelegate = new AddProgressDelegate(addProgress);
            setInfoDelegate = new SetInfoDelegate(setInfo);
        }

        private void btVarrer_Click(object sender, EventArgs e)
        {
            if (t != null)
                t.Abort();
            progressBar1.Value = 0;
            //ProcessarOrigemEDestino pod = new ProcessarOrigemEDestino(this);
            pod = new ProcessarOrigemEDestinoThree(tbOrigem.Text, tbDestino.Text, this);
            t = new Thread(pod.varrer);
            t.Start();

        }

        public void addProgress(int valor)
        {
            if (progressBar1.InvokeRequired)
            {
                this.Invoke(addProgressDelegate, new Object[] { valor });
            }
            else
            {
                if (progressBar1.Value + valor < 100)
                    progressBar1.Value += valor;
                else
                    progressBar1.Value = 100;
            }
        }

        public void setInfo(string info)
        {
            if (lbInfo.InvokeRequired)
            {
                this.Invoke(setInfoDelegate, new Object[] { info });
            }
            else
            {
                lbInfo.Text = info;
            }
        }

        private void btBackup_Click(object sender, EventArgs e)
        {
            if (!pod.rodando)
            {
                progressBar1.Value = 0;
                pod.executar();
            }
        }
    }
}
