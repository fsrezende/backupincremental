﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BackupIncremental.v._01
{
    class ItemBackup
    {
        public string fullName;
        public string name;
        public DateTime ultimaAlteracao;
        public bool ehDiretorio;

        public ItemBackup(string fullName, string name, DateTime ultimaAlteracao, bool ehDiretorio)
        {
            this.fullName = fullName;
            this.name = name;
            this.ultimaAlteracao = ultimaAlteracao;
            this.ehDiretorio = ehDiretorio;
        }

        internal ListViewItem ToListViewItem(DateTime dtDestino)
        {
            ListViewItem ret = new ListViewItem(fullName);
            ret.SubItems.Add(ultimaAlteracao.ToString());
            ret.SubItems.Add(dtDestino.ToString());
            ret.Tag = this;

            return ret;
        }

        public override bool Equals(Object obj)
        {
            ItemBackup ibObj = obj as ItemBackup;

            if (ibObj == null)
                return false;
            else
                return (ibObj.fullName.CompareTo(fullName) == 0);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool IsIdentical(Object obj)
        {
            ItemBackup ibObj = obj as ItemBackup;

            if (ibObj == null)
                return false;
            else
                return (ibObj.fullName.CompareTo(fullName) == 0) &&
                       (ibObj.ultimaAlteracao == ultimaAlteracao);
        }

    }
}
