﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace BackupIncremental.v._01
{
    class ProcessarOrigemEDestino
    {
        DirectoryInfo diOrigem;
        DirectoryInfo diDestino;
        List<ItemBackup> lstOrigem = new List<ItemBackup>();
        List<ItemBackup> lstDestino = new List<ItemBackup>();
        List<ItemBackup> lstMerge = new List<ItemBackup>();

        FormPrincipal formPrincipal;

        public ProcessarOrigemEDestino(FormPrincipal formPrincipal)
        {
            this.formPrincipal = formPrincipal;
        }

        public void varrer()
        {
            DateTime dt = DateTime.Now;
            diOrigem = new DirectoryInfo(formPrincipal.tbOrigem.Text);
            formPrincipal.addProgress(5);
            formPrincipal.setInfo("diOrigem");

            diDestino = new DirectoryInfo(formPrincipal.tbDestino.Text);
            formPrincipal.addProgress(5);
            formPrincipal.setInfo("diDestino");

            Parallel.Invoke(
                new Action(addNodeOrigem),
                new Action(addNodeDestino)
            );
            formPrincipal.addProgress(40);
            formPrincipal.setInfo("addNode");

            //lstMerge.AddRange(lstOrigem.Where(p => !lstDestino.Any(l => l == p)));
            processaAlteracao(lstMerge, lstOrigem, lstDestino);

            formPrincipal.addProgress(50);
            formPrincipal.setInfo("finalizado em " + DateTime.Now.Subtract(dt).ToString());
        }

        private void addNodeOrigem()
        {
            getItensDir(diOrigem, lstOrigem, 0);
        }

        private void addNodeDestino()
        {
            getItensDir(diDestino, lstDestino, 0);
        }

        private void getItensDir(DirectoryInfo di, List<ItemBackup> lst, int nivel)
        {
            formPrincipal.setInfo("varre (" + nivel + "): " + di.FullName);
            lst.Add(new ItemBackup(di.FullName.Substring(3), di.Name, di.LastWriteTime, false));

            //if(nivel < 2)
            try
            {
                foreach (DirectoryInfo sdi in di.GetDirectories())
                {
                    getItensDir(sdi, lst, nivel + 1);
                }
            }
            catch (Exception)
            {
            }

            try
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    lst.Add(new ItemBackup(fi.FullName.Substring(3), fi.Name, fi.LastWriteTime, false));
                }
            }
            catch (Exception)
            {
            }
        }

        private void processaAlteracao(List<ItemBackup> lstMerge, List<ItemBackup> lstOrigem, List<ItemBackup> lstDestino)
        {
            string strOrigem = "merge: " + formPrincipal.tbOrigem.Text;
            Parallel.ForEach(lstOrigem, itemOri =>
                {
                    //processaAlteracaoItem(lstMerge, itemOri, lstDestino);
                    formPrincipal.setInfo(strOrigem + itemOri.fullName);
                    if(!lstDestino.Any(l => l == itemOri))
                        lstMerge.Add(itemOri);

                } // fecha expressão lambda
            ); // fecha metodo ForEach
        }
    }
}
