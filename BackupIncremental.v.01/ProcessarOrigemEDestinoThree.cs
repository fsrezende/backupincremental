﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Threading.Tasks;

namespace BackupIncremental.v._01
{
    class ProcessarOrigemEDestinoThree
    {
        string strOrigem;
        string strDestino;
        List<ItemBackup> lstMerge;
        List<string> lstErro;

        FormPrincipal formPrincipal;

        public bool rodando;

        public ProcessarOrigemEDestinoThree(string strOrigem, string strDestino, FormPrincipal formPrincipal)
        {
            rodando = true;
            this.formPrincipal = formPrincipal;
            this.strOrigem = strOrigem;
            this.strDestino = strDestino;
        }

        public void varrer()
        {
            rodando = true;
            DateTime dt = DateTime.Now;
            lstMerge = new List<ItemBackup>();
            getItensDir(new DirectoryInfo(strOrigem), lstMerge, 0);
            formPrincipal.addProgress(100);
            formPrincipal.setInfo("finalizado em " + DateTime.Now.Subtract(dt).ToString(@"hh\:mm\:ss") + " com " + lstMerge.Count + " arquivos.");
            rodando = false;
        }

        private void getItensDir(DirectoryInfo di, List<ItemBackup> lst, int nivel)
        {
            formPrincipal.setInfo("varre (" + nivel + "): " + di.FullName);
            //lst.Add(new ItemBackup(di.FullName.Substring(3), di.Name, di.LastWriteTime, false));


            //if(nivel < 2)
            try
            {
                int progressValue = 100 / (di.GetDirectories().Length + 1);
                foreach (DirectoryInfo sdi in di.GetDirectories())
                {
                    if (sdi.FullName.IndexOf('$') < 0)
                    {
                        if (Directory.Exists(sdi.FullName.Replace(strOrigem, strDestino)))
                            getItensDir(sdi, lst, nivel + 1);
                        else
                            lst.Add(new ItemBackup(sdi.FullName, sdi.Name, sdi.LastWriteTime, true));
                    }
                    if (nivel == 0)
                        formPrincipal.addProgress(progressValue);
                }
            }
            catch (Exception e)
            {
                lstErro.Add(e.Message);
            }

            try
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    if (fi.FullName.IndexOf('$') < 0)
                    {
                        if (!File.Exists(fi.FullName.Replace(strOrigem, strDestino)))
                            lst.Add(new ItemBackup(fi.FullName, fi.Name, fi.LastWriteTime, false));
                        else
                        {
                            FileInfo fo = new FileInfo(fi.FullName.Replace(strOrigem, strDestino));
                            if (fo.Length != fi.Length ||
                                fo.LastWriteTime != fi.LastWriteTime)
                                lst.Add(new ItemBackup(fi.FullName, fi.Name, fi.LastWriteTime, false));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                lstErro.Add(e.Message);
            }
        }

        public void executar()
        {
            DateTime dt = DateTime.Now;
            lstErro = new List<string>();
            int progressValue = 100 / lstMerge.Count;
            //Parallel.ForEach(lstMerge, ib =>
            //{
            //    formPrincipal.setInfo("executa: " + ib.fullName);
            //    if (ib.ehDiretorio)
            //        copyDirectory(ib.fullName, strOrigem, strDestino);
            //    else
            //        copyFile(ib.fullName, strOrigem, strDestino);
            //    formPrincipal.addProgress(progressValue);

            //} // fecha expressão lambda
            //); // fecha metodo ForEach

            foreach (ItemBackup ib in lstMerge)
            {
                formPrincipal.setInfo("executa: " + ib.fullName);
                if (ib.ehDiretorio)
                    copyDirectory(ib.fullName, strOrigem, strDestino);
                else
                    copyFile(ib.fullName, strOrigem, strDestino);
                formPrincipal.addProgress(progressValue);
            }
            formPrincipal.addProgress(100);
            formPrincipal.setInfo("finalizado em " + DateTime.Now.Subtract(dt).ToString(@"hh\:mm\:ss") +
                                  " com " + lstErro.Count + " erros!");
        }

        public void executar2()
        {
            DateTime dt = DateTime.Now;
            DirectoryInfo target = new DirectoryInfo(strDestino);
            int progressValue = 100 / lstMerge.Count;
            foreach (ItemBackup ib in lstMerge)
            {
                formPrincipal.setInfo("executa: " + ib.fullName);
                if (ib.ehDiretorio)
                    copyDirectory(new DirectoryInfo(ib.fullName), target);
                else
                    copyFile(new FileInfo(ib.fullName), target);
                formPrincipal.addProgress(progressValue);
            }
            formPrincipal.addProgress(100);
            formPrincipal.setInfo("finalizado em " + DateTime.Now.Subtract(dt).ToString(@"hh\:mm\:ss"));
        }

        private void copyDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
                copyDirectory(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                copyFile(file, target);
        }

        private void copyDirectory(string dir, string origem, string destino)
        {
            try
            {
                formPrincipal.setInfo("criando " + dir);
                Directory.CreateDirectory(dir.Replace(origem, destino));
                foreach (string subDir in Directory.EnumerateDirectories(dir))
                    copyDirectory(subDir, origem, destino);
                foreach (string file in Directory.EnumerateFiles(dir))
                    copyFile(file, origem, destino);
            }
            catch (Exception e)
            {
                lstErro.Add(e.Message);
            }
        }

        private void copyFile(string file, string origem, string destino)
        {
            try
            {
                formPrincipal.setInfo("copiando " + file);
                File.Copy(file, file.Replace(origem, destino), true);
            }
            catch (Exception e)
            {
                lstErro.Add(e.Message);
            }
        }

        private void copyFile(FileInfo file, DirectoryInfo target)
        {
            file.CopyTo(Path.Combine(target.FullName, file.Name), true);
        }

    }
}
