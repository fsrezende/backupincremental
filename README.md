BackupIncremental
=================

Sistema para automatizar o meu backup de arquivos utilizando uma estrutura simples de pastas/diretórios para replicar os arquivos de origem no repositório de destino.

##[Pendente]
* Arquivo de configuração - Origem e Destino;
* Comparação de mais de dois diretórios;
* Comparação do conteúdo dos arquivos;
* Backup seletivo, configurado por dispositivo;
* Backup automático;
* Usar nuvem como destino;
* 
* 

###[21/10/2014]
* Criar uma classe para identificação do Arquivo/Diretório: ItemBackup;
* Varre o repositório de origem e verifica se os arquivos existem ou se foram alterados;
* Executa a cópia num evento separado da varredura;
* Exibe uma interface gráfica simples e amigável com campos de origem, destino, info e progresso;
